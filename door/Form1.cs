﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;
using System.Media;
using System.Threading;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Data.OleDb;
using System.Text;
using System.IO;

namespace door
{
    public partial class Form1 : Form
    {
        public delegate void BarcodeReceivedEventHandler(object sender, BarcodeReceivedEventArgs e);
        public class BarcodeReceivedEventArgs : EventArgs
        {

            private string _barcode;

            public BarcodeReceivedEventArgs(string Barcode)
            {

                _barcode = Barcode;

            }

            public string Barcode { get { return _barcode; } }

        }
        public event BarcodeReceivedEventHandler BarcodeReceived;

        const char ScannerEndCharacter = '\r';

        private StringBuilder scannedCharacters;

        List<string> dataItems = new List<string>();

        //ตัวแปรของกล้อง
        VideoCapture capture;
        Mat frame;
        Bitmap image;
        private Thread camera;
        bool isCameraRunning = false;

        //ตัวแปรของ NFC
        int retCode;
        int hCard;
        int hContext;
        int Protocol;
        public bool connActive = false;
        string readername = "ACS ACR122 0";      // change depending on reader
        public byte[] SendBuff = new byte[263];
        public byte[] RecvBuff = new byte[263];
        public int SendLen, RecvLen, nBytesRet, reqType, Aprotocol, dwProtocol, cbPciLength;
        public Card.SCARD_READERSTATE RdrState;
        public Card.SCARD_IO_REQUEST pioSendRequest;
        private BackgroundWorker _worker;
        private Card.SCARD_READERSTATE[] states;

        //State ของ Card
        internal enum SmartcardState
        {
            None = 0,
            Inserted = 1,
            Ejected = 2
        }

        //การแสดงผล
        public Form1()
        {
            InitializeComponent();
            StartTimer();
            SelectDevice();
            establishContext();
            Watch();
            scannedCharacters = new StringBuilder();
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(Form1_KeyPress); //เพื่อตรวจสอบการอ่านของเครื่องอ่าน 

            this.BarcodeReceived += new BarcodeReceivedEventHandler(Form1_BarcodeReceived); //หากมีการอ่านของบาร์โค้ดจะเรียกใช้ฟังก์ชันในการอ่าน
        }
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)//ฟังก์ชันที่ตรวจสอบการอ่านของเครื่องอ่าน
        {
            if (e.KeyChar == 13)    //การแสกนจะตัดที่การ Enter (keychar==13 คือ Enter)
            {
                e.Handled = true;
                OnBarcodeRecieved(new BarcodeReceivedEventArgs(scannedCharacters.ToString()));
                scannedCharacters.Remove(0, scannedCharacters.Length);
            }

            else
            {
                scannedCharacters.Append(e.KeyChar);
                e.Handled = false;
            }
        }
        protected virtual void OnBarcodeRecieved(BarcodeReceivedEventArgs e)//ฟังก์ชันที่ใช้จัการหลังตรวจสอบว่าเครื่องอ่านทำการอ่านได้
        {
            if (BarcodeReceived != null)
                BarcodeReceived(this, e); //ส่งค่ากลับไปที่ Form1 ก่อน Form1 จะเรียกใช้ฟัง์ชัน Form1_BarcodeReceived

        }

        void Form1_BarcodeReceived(object sender, BarcodeReceivedEventArgs e) //ฟังก์ชันอ่านบาร์โค้ด
        {
            //นำค่าที่ได้จากการอ่านบาร์โค้ดไป query ใน database
            //MessageBox.Show("Barcode scanned: " + e.Barcode);
            if (e.Barcode.Length == 10)  //หากอ่านบาร์โค้ดได้ทั้งหมด 10 ตัวอักษร (ถ้าเพิ่มความยาวของบาร์โค้ด ต้องแก้เลข 10)
            {
                String str = null;
                string esString = null;
                string typeString = null;
                string idString = null;
                string keyString = null;
                str = e.Barcode;
                Console.WriteLine("***************" + str);
                //ทำการ subString เพื่อแยก code ที่อ่านได้เป็นส่วนๆ เช่น id ประเภทของticket เป็นต้น
                esString = str.Substring(0, 2);
                typeString = str.Substring(2, 1);
                idString = str.Substring(3, 4);
                keyString = str.Substring(7, 3);
                //นำค่าที่ได้ไปค้นหาในฐานข้อมูล
                PostGreSQL pgTest = new PostGreSQL();
                dataItems = pgTest.QueryToDay(typeString, idString, keyString);
                tbDataItems.Clear(); //เคลียพื้นที่แสดงผล

                for (int i = 0; i < dataItems.Count; i++) //แสดงผลลัพธ์ที่ได้
                {
                    tbDataItems.Text += dataItems[i];
                    tbDataItems.ScrollToCaret();
                }
                if (dataItems[0] != "")  //หากผลลัพธ์มีในฐานข้อมูลจะทำการเล่นเสียงและ logข้อมูล
                {
                    PlaySound1();   //เล่นเสียงยินดีต้อนรับ
                    //logBarcode type เด็ก ผู้ใหญ่ ID Key
                    LoggerBarcode("UserType:" + typeString + "    ID: " + idString + "    Key: " + keyString);
                }
                else  //หากผลลัพธ์ไม่มีในฐานข้อมูล
                {
                    PlaySound2();   //เล่นเสียงไม่พบข้อมูล
                }
            }
            else //หากอ่านบาร์โค้ด ไม่ได้ความยาว 10 จะเล่นเสียงไม่พบข้อมูล
            {
                tbDataItems.Clear();  //เคลียพื้นที่แสดงผล
                PlaySound2();    //เล่นเสียงไม่พบข้อมูล
            }

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            CaptureCamera();
            isCameraRunning = true;
        }
        //วนลูปเช็คสถานะของ card ว่ามีบัตรอยู่หรือไม่
        private void WaitChangeStatus(object sender, DoWorkEventArgs e)
        {
            while (!e.Cancel)
            {
                int nErrCode = Card.SCardGetStatusChange(hContext, 1000, ref states[0], 1);
                //ตรวจว่ามีการเปลี่ยนสถานะล่าสุดหรือไม่
                if ((this.states[0].RdrEventState & 2) == 2)
                {
                    //ตรวจว่ามีอะไรเปลี่ยน
                    SmartcardState state = SmartcardState.None;
                    if ((this.states[0].RdrEventState & 32) == 32 && (this.states[0].RdrCurrState & 32) != 32)
                    {
                        //มี card อยู่
                        state = SmartcardState.Inserted;
                    }
                    else if ((this.states[0].RdrEventState & 16) == 16 && (this.states[0].RdrCurrState & 16) != 16)
                    {
                        //ไม่มี card อยู่
                        state = SmartcardState.Ejected;
                    }
                    if (state != SmartcardState.None && this.states[0].RdrCurrState != 0)
                    {
                        switch (state)
                        {
                            case SmartcardState.Inserted:   //เมื่อมี card
                                {
                                    //MessageBox.Show("Card inserted");
                                    read();
                                    break;
                                }
                            case SmartcardState.Ejected:    //เมื่อไม่มี card
                                {
                                    //MessageBox.Show("Card ejected");
                                    Disconnect();
                                    break;
                                }
                            default:
                                {
                                    //MessageBox.Show("Some other state...");
                                    break;
                                }
                        }
                    }
                    //อัพเดตสถานะล่าสุดสำหรับการตรวจสอบครั้งถัดไป
                    this.states[0].RdrCurrState = this.states[0].RdrEventState;
                }
            }
        }

        //อ่านค่าของบัตร
        private void read()
        {
        
            //กำหนดตำแหน่งฐานข้อมูล
           
            bool connectActive = connectCard();

            if (connectActive)  //ตรวจสอบว่ามีการเชื่อมต่อหรือไม่
            {
                string StdID = verifyCard("1");      //เก็บรหัส น.ศ ที่อยู่ใน blockที่ 1 ของบัตรโดยนำไปตรวจสอบกับ key แล้วเก็บในตัวแปร StdID
                Console.WriteLine(">>>>>>>>>>" + StdID);
                string cardUID = getcardUID();      //เก็บค่า UID ในตัวแปร cardUID
                UpdatingTextBox(cardUID);           //แสดงค่า UID ลงใน textbox
                UpdatingTextBox1(StdID);             //แสดงค่า รหัส น.ศ ลงใน text box

                UpdatingTxtAll("   Student ID    : " + StdID);   //แสดงรหัส น.ศ.

                //เชื่อมต่อฐานข้อมูล
                if (StdID != "FailAuthentication" && StdID != "Error") //ถ้าตรวจสอบ รหัส น.ศ กับ UID แล้วเป็บตามเงื่อนไข จะเข้า try แต่ถ้าไม่ จะเข้า catch
                {
                    string connetionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\nfc-and-barcode\\database\\REPORT_CIRMEMBER.mdb;";
                    OleDbConnection con = new OleDbConnection(connetionString);

                    try
                    {
                        con.Open();
                        //เชื่อต่อกับฐานข้อมูล
                        OleDbCommand cmd = new OleDbCommand("SELECT * FROM public_report_cir_member1 WHERE member_code = '" + StdID + "' ", con);
                        OleDbDataReader dataReader = cmd.ExecuteReader();   

                        //อ่านข้อมูลในฐานข้อมูล
                        if (dataReader.Read())
                        {
                            //แสดง ชื่อ และนามสกุล ลงในtextbox
                            UpdatingTxtFirstName(dataReader[1].ToString());
                            UpdatingTxtSureName(dataReader[2].ToString());
                            UpdatingTxtDate(dataReader[8].ToString().Substring(0, 10));

                            UpdatingTxtAll2("   FirstName     : " + dataReader[1].ToString());
                            UpdatingTxtAll3("   SureName    : " + dataReader[2].ToString());
                            UpdatingTxtAll4("   ExpireDate    : " + dataReader[8].ToString().Substring(0, 10));

                            
                            
 
                            string dateNow = DateTime.Now.ToString(); //วันปัจจุบัน
                            string expireDate = dataReader[8].ToString(); //วันหมดอายุในบัตร

                            //เช็ควันหมดอายุของบัตร
                            if (DateTime.Parse(expireDate) < DateTime.Parse(dateNow))
                            {
                                Image imageCheck2 = null;
                                imageCheck2 = Image.FromFile(@"\nfc-and-barcode\picture\expired.jpg");    //แสดงรูปภาพ Nodata
                                pictureBox3.Image = imageCheck2;
                                pictureBox2.Image = imageCheck2;
                                PlaySound3();
                            }
                            else
                            {
                                //logCard รหัส ชื่อ สกุล วันเวลา
                                LoggerCard("stdID: " + StdID + "   FirstName: " + dataReader[1].ToString() + "     SureName: " + dataReader[2].ToString());
                                Console.WriteLine("################" + StdID);
                                //ถ้ามีรหัส น.ศ ฐานข้อมูลจะทำการดึงรูปภาพจากฐานข้อมูลมาแสดง
                                Image imageCheck1 = null;
                                imageCheck1 = Image.FromFile(@"\nfc-and-barcode\picture\" + StdID + ".jpg");
                                 pictureBox3.Image = imageCheck1;
                                //ถ่ายภาพ
                                useCapture();
                                //เล่นเสียงยินดีย้อนรับค่ะ
                                PlaySound1();
                            }

                        }
                        else //ไม่พบข้อมูลในกรณีไม่มีข้อมูลในฐานข้อมูล
                        {
                            PrintNoData();
                        }
                        //เมื่อ read แล้ว จะต้อง close ทุกครั้ง
                        dataReader.Close();
                        cmd.Dispose();
                        con.Close();
                    }
                    catch (Exception) //ทำงานเมื่อ เกิด Exception
                    {
                        PrintNoData();
                    }
                }
                else //ยินยันตัวตนไม่สำเร็จหรือมีข้อผิดพลาด
                {
                    PrintNoData();
                }

            }
        }

        //log ไฟล์แสดงประวัติการเข้าใช้งาน
        
        public static void VerifyDir(string path)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.Exists)
                {
                    dir.Create();
                }
            }
            catch { }
        }
        public static void LoggerCard(string lines)
        {
            string path = @"\nfc-and-barcode\LogCards\";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + " " + DateTime.Now.Month.ToString() + " " + DateTime.Now.Year.ToString() + "_Cards.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString() + ": " + lines);
                file.Close();
            }
            catch (Exception) { }
        }
        public static void LoggerBarcode(string lines)
        {
            string path = @"\nfc-and-barcode\LogBarcode\";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + " " + DateTime.Now.Month.ToString() + " " + DateTime.Now.Year.ToString() + "_Barcode.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString() + ": " + lines);
                file.Close();
            }
            catch (Exception) { }
        }

        private void PrintNoData()
        {
            //แสดงว่า ไม่พบข้อมูล
            UpdatingTxtFirstName("No Data");
            UpdatingTxtSureName("No Data");
            UpdatingTxtAll2("   FirstName     : No Data");
            UpdatingTxtAll3("   SureName    : No Data ");
            UpdatingTxtAll4("   ExpireDate    : No Data ");
            Image imageCheck2 = null;
            imageCheck2 = Image.FromFile(@"\nfc-and-barcode\picture\nodata.jpg");    //แสดงรูปภาพ Nodata
            pictureBox3.Image = imageCheck2;
            pictureBox2.Image = imageCheck2;
            PlaySound2();     //เล่นเสียงไม่พบข้อมูลค่ะ
        }
        //การทำงานของการแคปภาพจากกล้อง
        private void CaptureCamera()
        {
            camera = new Thread(new ThreadStart(CaptureCameraCallback));
            camera.Start();
        }
        private void CaptureCameraCallback()
        {
            frame = new Mat();
            capture = new VideoCapture(0);
            capture.Open(0);

            if (capture.IsOpened())
            {
                while (isCameraRunning)
                {

                    capture.Read(frame);
                    image = BitmapConverter.ToBitmap(frame);
                    if (pictureBox1.Image != null)
                    {
                        pictureBox1.Image.Dispose();
                    }
                    pictureBox1.Image = image;
                }
            }
        }
        private void useCapture()
        {
            if (isCameraRunning)
            {
                // Take snapshot of the current image generate by OpenCV in the Picture Box
                Image exportCapture = null;
                exportCapture = pictureBox1.Image;
                //Bitmap snapshot = new Bitmap(exportCapture);
                string name_pic = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                // Save in some directory
                exportCapture.Save(string.Format(@"\nfc-and-barcode\capture\" + name_pic + ".jpg", Guid.NewGuid()), ImageFormat.Jpeg);
                Image imageTest = null;
                imageTest = Image.FromFile(@"\nfc-and-barcode\capture\" + name_pic + ".jpg");
                pictureBox2.Image = imageTest;
            }
            else
            {
                Console.WriteLine("Cannot take picture if the camera isn't capturing image!");
            }
        }

        //ตั้งค่าเวลาเป็นเวลาปัจจุบัน
        System.Windows.Forms.Timer tmr = null;
        private void StartTimer()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(tmr_Tick);
            tmr.Enabled = true;
        }
        //แสดงเวลาลงใน textbox
        void tmr_Tick(object sender, EventArgs e)
        {
            textBox2.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }
        //เสียงยินดีต้อนรับค่ะ
        private void PlaySound1()
        {
            SoundPlayer player1 = new SoundPlayer();
            player1.SoundLocation = @"\nfc-and-barcode\sound\ยินดีต้อนรับค่ะ.wav";
            player1.Play();
        }
        //เสียงไม่พบข้อมูลค่ะ
        private void PlaySound2()
        {
            SoundPlayer player2 = new SoundPlayer();
            player2.SoundLocation = @"\nfc-and-barcode\sound\ไม่พบข้อมูลค่ะ.wav";
            player2.Play();
        }
        //เสียงบัตรของคุณหมดอายุแล้วค่ะ
        private void PlaySound3()
        {
            SoundPlayer player3 = new SoundPlayer();
            player3.SoundLocation = @"\nfc-and-barcode\sound\บัตรของคุณหมดอายุแล้วค่ะ.wav";
            player3.Play();
        }

        //ฟังก์ชั่นอ่านค่า UID ของบัตร
        private string getcardUID()//only for mifare 1k cards
        {
            string cardUID = "";
            byte[] receivedUID = new byte[256];
            Card.SCARD_IO_REQUEST request = new Card.SCARD_IO_REQUEST();
            request.dwProtocol = Card.SCARD_PROTOCOL_T1;
            request.cbPciLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Card.SCARD_IO_REQUEST));
            byte[] sendBytes = new byte[] { 0xFF, 0xCA, 0x00, 0x00, 0x00 }; //get UID command      for Mifare cards
            int outBytes = receivedUID.Length;
            int status = Card.SCardTransmit(hCard, ref request, ref sendBytes[0], sendBytes.Length, ref request, ref receivedUID[0], ref outBytes);

            if (status != Card.SCARD_S_SUCCESS)
            {
                cardUID = "Error";

            }
            else
            {
                cardUID = BitConverter.ToString(receivedUID.Take(4).ToArray()).Replace("-", string.Empty).ToLower();
            }
            return cardUID;
        }
        //ฟังก์ชั่นตรวจสอบบัตรแต่ล่ะ Block 
        public string verifyCard(String Block)
        {
            string value = "";
            if (connectCard())
            {
                value = readBlock(Block);
            }
            
            value = value.Split(new char[] { '\0' }, 2, StringSplitOptions.None)[0].ToString();
            return value;
        }
        //อ่านค่าที่อยู่ใน Block แต่ล่ะ Block ของบัตร
        public string readBlock(String Block)
        {
            string tmpStr = "";
            int indx;

            if (authenticateBlock(Block))
            {
                ClearBuffers();
                SendBuff[0] = 0xFF; // CLA 
                SendBuff[1] = 0xB0;// INS
                SendBuff[2] = 0x00;// P1
                SendBuff[3] = (byte)int.Parse(Block);// P2 : Block No.
                SendBuff[4] = (byte)int.Parse("16");// Le

                SendLen = 5;
                RecvLen = SendBuff[4] + 2;

                retCode = SendAPDUandDisplay(2);

                if (retCode == -200)
                {
                    return "outofrangeexception";
                }

                if (retCode == -202)
                {
                    return "BytesNotAcceptable";
                }

                if (retCode != Card.SCARD_S_SUCCESS)
                {
                    return "FailRead";
                }

                // Display data in text format
                for (indx = 0; indx <= RecvLen - 1; indx++)
                {
                    tmpStr = tmpStr + Convert.ToChar(RecvBuff[indx]);
                }

                return (tmpStr);
            }
            else
            {
               return "FailAuthentication";
            }
        }
        // block authentication
        private bool authenticateBlock(String block)
        {
            ClearBuffers();
            SendBuff[0] = 0xFF;                         // CLA
            SendBuff[2] = 0x00;                         // P1: same for all source types 
            SendBuff[1] = 0x86;                         // INS: for stored key input
            SendBuff[3] = 0x00;                         // P2 : Memory location;  P2: for stored key input
            SendBuff[4] = 0x05;                         // P3: for stored key input
            SendBuff[5] = 0x01;                         // Byte 1: version number
            SendBuff[6] = 0x00;                         // Byte 2
            SendBuff[7] = (byte)int.Parse(block);       // Byte 3: sectore no. for stored key input
            SendBuff[8] = 0x60;                         // Byte 4 : Key A for stored key input
            SendBuff[9] = (byte)int.Parse("1");         // Byte 5 : Session key for non-volatile memory

            SendLen = 0x0A;
            RecvLen = 0x02;

            retCode = SendAPDUandDisplay(0);

            if (retCode != Card.SCARD_S_SUCCESS)
            {
                //MessageBox.Show("FAIL Authentication!");
                return false;
            }

            return true;
        }
        // clear memory buffers
        private void ClearBuffers()
        {
            long indx;

            for (indx = 0; indx <= 262; indx++)
            {
                RecvBuff[indx] = 0;
                SendBuff[indx] = 0;
            }
        }
        public string Transmit(byte[] buff)
        {
            string tmpStr = "";
            int indx;

            ClearBuffers();

            for (int i = 0; i < buff.Length; i++)
            {
                SendBuff[i] = buff[i];
            }
            SendLen = 5;
            RecvLen = SendBuff[SendBuff.Length - 1] + 2;

            retCode = SendAPDUandDisplay(2);


            // Display data in text format
            for (indx = 0; indx <= RecvLen - 1; indx++)
            {
                tmpStr = tmpStr + Convert.ToChar(RecvBuff[indx]);
            }

            return tmpStr;
        }

        // send application protocol data unit : communication unit between a smart card reader and a smart card
        private int SendAPDUandDisplay(int reqType)
        {
            int indx;
            string tmpStr = "";

            pioSendRequest.dwProtocol = Aprotocol;
            pioSendRequest.cbPciLength = 8;

            //Display Apdu In
            for (indx = 0; indx <= SendLen - 1; indx++)
            {
                tmpStr = tmpStr + " " + string.Format("{0:X2}", SendBuff[indx]);
            }

            retCode = Card.SCardTransmit(hCard, ref pioSendRequest, ref SendBuff[0],
                                 SendLen, ref pioSendRequest, ref RecvBuff[0], ref RecvLen);

            if (retCode != Card.SCARD_S_SUCCESS)
            {
                return retCode;
            }

            else
            {
                try
                {
                    tmpStr = "";
                    switch (reqType)
                    {
                        case 0:
                            for (indx = (RecvLen - 2); indx <= (RecvLen - 1); indx++)
                            {
                                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            if ((tmpStr).Trim() != "90 00")
                            {
                                //MessageBox.Show("Return bytes are not acceptable.");
                                return -202;
                            }

                            break;

                        case 1:

                            for (indx = (RecvLen - 2); indx <= (RecvLen - 1); indx++)
                            {
                                tmpStr = tmpStr + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            if (tmpStr.Trim() != "90 00")
                            {
                                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            else
                            {
                                tmpStr = "ATR : ";
                                for (indx = 0; indx <= (RecvLen - 3); indx++)
                                {
                                    tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                                }
                            }

                            break;

                        case 2:

                            for (indx = 0; indx <= (RecvLen - 1); indx++)
                            {
                                tmpStr = tmpStr + " " + string.Format("{0:X2}", RecvBuff[indx]);
                            }

                            break;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    return -200;
                }
            }
            return retCode;
        }

        //เชื่อต่อกับอุปกรณ์
        public void SelectDevice()
        {
            List<string> availableReaders = this.ListReaders();
            this.RdrState = new Card.SCARD_READERSTATE();
            readername = availableReaders[0].ToString();//selecting first device
            this.RdrState.RdrName = readername;
        }
        public List<string> ListReaders()
        {
            int ReaderCount = 0;
            List<string> AvailableReaderList = new List<string>();

            //Make sure a context has been established before 
            //retrieving the list of smartcard readers.
            retCode = Card.SCardListReaders(hContext, null, null, ref ReaderCount);
            if (retCode != Card.SCARD_S_SUCCESS)
            {
                MessageBox.Show(Card.GetScardErrMsg(retCode));
                //connActive = false;
            }

            byte[] ReadersList = new byte[ReaderCount];

            //Get the list of reader present again but this time add sReaderGroup, retData as 2rd & 3rd parameter respectively.
            retCode = Card.SCardListReaders(hContext, null, ReadersList, ref ReaderCount);
            if (retCode != Card.SCARD_S_SUCCESS)
            {
                MessageBox.Show(Card.GetScardErrMsg(retCode));
            }

            string rName = "";
            int indx = 0;
            if (ReaderCount > 0)
            {
                // Convert reader buffer to string
                while (ReadersList[indx] != 0)
                {

                    while (ReadersList[indx] != 0)
                    {
                        rName = rName + (char)ReadersList[indx];
                        indx = indx + 1;
                    }

                    //Add reader name to list
                    AvailableReaderList.Add(rName);
                    rName = "";
                    indx = indx + 1;

                }
            }
            return AvailableReaderList;
        }

        //สร้างการเชื่อต่อ
        internal void establishContext()
        {
            retCode = Card.SCardEstablishContext(Card.SCARD_SCOPE_SYSTEM, 0, 0, ref hContext);
            if (retCode != Card.SCARD_S_SUCCESS)
            {
               
                connActive = false;
                return;
            }
        }

        //ฟังก์ชั่นไม่เชื่อต่อกับบัตร
        public void Disconnect()
        {
            if (connActive)
            {
                retCode = Card.SCardDisconnect(hCard, Card.SCARD_UNPOWER_CARD);
                //เมื่อไม่มีการเชื่อต่อจะทำการอัพเดท textbox ให้ว่างเปล่า
                UpdatingTextBox("");
                UpdatingTextBox1("");
                UpdatingTxtFirstName("");
                UpdatingTxtSureName("");
                UpdatingTxtDate("");
            }
            // retCode = Card.SCardReleaseContext(hCard);
        }

        //คืนค่าเป็น true เมื่อ เชื่อมบัตร flase เมื่อไม่ได้เชื่อม
        public bool connectCard()
        {
            connActive = true;

            retCode = Card.SCardConnect(hContext, readername, Card.SCARD_SHARE_SHARED,
                      Card.SCARD_PROTOCOL_T0 | Card.SCARD_PROTOCOL_T1, ref hCard, ref Protocol);

            if (retCode != Card.SCARD_S_SUCCESS)
            {

                connActive = false;
                return false;
            }
            return true;
        }

        //ฟังก์ชั่นการทำงานทำงานแบบ backgroundworker
        public void Watch()
        {
            this.RdrState = new Card.SCARD_READERSTATE();
            this.RdrState.RdrName = readername;

            states = new Card.SCARD_READERSTATE[1];
            states[0] = new Card.SCARD_READERSTATE();
            states[0].RdrName = readername;
            states[0].UserData = 0;
            states[0].RdrCurrState = Card.SCARD_STATE_EMPTY;
            states[0].RdrEventState = 0;
            states[0].ATRLength = 0;
            states[0].ATRValue = null;
            this._worker = new BackgroundWorker();
            this._worker.WorkerSupportsCancellation = true;
            this._worker.DoWork += WaitChangeStatus;
            this._worker.RunWorkerAsync();
        }

        //delegate เพื่อแสดงข้อความใน textbox
        delegate void TextBoxDelegate(string message);
        //textbox Uid
        public void UpdatingTextBox(string msg)
        {
            if (this.txtUid.InvokeRequired)
                this.txtUid.Invoke(new TextBoxDelegate(UpdatingTextBox), new object[] { msg });

            else
            {
                this.txtUid.Text = msg;
                
            }
        }
        //textbox รหัสนักศึกษา
        public void UpdatingTextBox1(string msg)
        {
            if (this.txtStdID.InvokeRequired)
                this.txtStdID.Invoke(new TextBoxDelegate(UpdatingTextBox1), new object[] { msg });

            else
                this.txtStdID.Text = msg;
        }
        //textbox ชื่อ
        public void UpdatingTxtFirstName(string msg)
        {
            if (this.txtFirstName.InvokeRequired)
                this.txtFirstName.Invoke(new TextBoxDelegate(UpdatingTxtFirstName), new object[] { msg });

            else
                this.txtFirstName.Text = msg;
        }
        //textbox สกุล
        public void UpdatingTxtSureName(string msg)
        {
            if (this.txtSurname.InvokeRequired)
                this.txtSurname.Invoke(new TextBoxDelegate(UpdatingTxtSureName), new object[] { msg });

            else
                this.txtSurname.Text = msg;
        }
        //textbox วันเวลา
        public void UpdatingTxtDate(string msg)
        {
            if (this.txtDate.InvokeRequired)
                this.txtDate.Invoke(new TextBoxDelegate(UpdatingTxtDate), new object[] { msg });

            else
                this.txtDate.Text = msg;
        }
        //textbox แสดง วัน
        public void UpdatingTxtAll4(string msg)
        {
            if (this.txtDisplayDate.InvokeRequired)
                this.txtDisplayDate.Invoke(new TextBoxDelegate(UpdatingTxtAll4), new object[] { msg });

            else
                this.txtDisplayDate.Text = msg;
        }
        //textbox แสดง สกุล
        public void UpdatingTxtAll3(string msg)
        {
            if (this.txtDisplaySurname.InvokeRequired)
                this.txtDisplaySurname.Invoke(new TextBoxDelegate(UpdatingTxtAll3), new object[] { msg });

            else
                this.txtDisplaySurname.Text = msg;
        }
        //textbox แสดง ชื่อ
        public void UpdatingTxtAll2(string msg)
        {
            if (this.txtDisplayFirstName.InvokeRequired)
                this.txtDisplayFirstName.Invoke(new TextBoxDelegate(UpdatingTxtAll2), new object[] { msg });

            else
                this.txtDisplayFirstName.Text = msg;
        }
        //textbox แสดง รหัสนักศึกษา
        public void UpdatingTxtAll(string msg)
        {
            if (this.txtDisplayStdID.InvokeRequired)
                this.txtDisplayStdID.Invoke(new TextBoxDelegate(UpdatingTxtAll), new object[] { msg });

            else
                this.txtDisplayStdID.Text = msg;
        }

        private void txtBoxAll3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBoxAll2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void txtSurename_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBoxAll1_TextChanged(object sender, EventArgs e)
        {

        }

     
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            isCameraRunning = false;
        }

        private void txtDate_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDisplayDate_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbDataItems_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUid_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }


    }
}
