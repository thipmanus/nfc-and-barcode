﻿using System;
using System.Windows.Forms;
using Npgsql;
using System.Collections.Generic;

namespace door  //ใช้ในการติดต่อฐานข้อมูล PGadmin(API)
{
    public class PostGreSQL
    {
        List<string> dataItems = new List<string>();

        public void PostgreSQL()
        {
        }

        public List<string> QueryToDay(String type, String id, String key) //ฟังก์ชันค้นหาข้อมูล ticket ที่ได้ออกวันนี้
        {
            try
            {
                string connstring = "Server=192.168.26.19; Port=5432; User Id=ticket; Password=abc123; Database=ticket;";
                NpgsqlConnection connection = new NpgsqlConnection(connstring);
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM tkstamp WHERE time_stamp >=' " + DateTime.Now.ToString("yyyyMMdd") + "' AND user_type = '" + type + "'  AND id = '" + id + "' AND key_secret = '" + key + "' ", connection);
                NpgsqlDataReader dataReader = command.ExecuteReader();
                //ใช้ในการ add ข้อมูลที่ query มาลงในตัวแปร dataItems
                for (int i = 0; dataReader.Read(); i++) //ใช้ในการ add ข้อมูลที่ query มาลงในตัวแปร dataItems
                {
                    dataItems.Add("User Type : " + dataReader[2].ToString() + "\r\n" +
                        "ID : " + dataReader[1].ToString() + "\r\n" +
                        "Price : " + dataReader[3].ToString() + " Bath" + "\r\n" +
                        "Key : " + dataReader[6].ToString() + "\r\n" +
                        "Time Stamp : " + dataReader[5].ToString() + "\r\n" + "---------------------------------------------------------" + "\r\n");
                }
                dataItems.Add(""); //add เพื่อไม่ให้ Listเป็น NULL ใช้ในการตรวจสอบว่าค้นหาเจอหรือไม่
                connection.Close();
                return dataItems;
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.ToString());
                throw;
            }
        }


        /*
        public List<string> QueryShowData() //ฟังก์ชันที่เรียกใช้เพื่อแสดงผลข้อมูล
        {
           try
           {
              string connstring = "Server=192.168.26.19; Port=5432; User Id=ticket; Password=abc123; Database=ticket;";
              NpgsqlConnection connection = new NpgsqlConnection(connstring);
              connection.Open();
              NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM tkstamp ORDER BY norow DESC LIMIT 100", connection); // ทำการอ่าน 100 ข้อมูลล่าสุด
              NpgsqlDataReader dataReader = command.ExecuteReader();
              for (int i=0; dataReader.Read(); i++) //ใช้ในการ add ข้อมูลที่ query มาลงในตัวแปร dataItems
              {
                 dataItems.Add("User Type : " + dataReader[2].ToString() + "\r\n" +
                               "ID : " + dataReader[1].ToString() + "\r\n" +
                               "Price : " + dataReader[3].ToString() +" Bath" + "\r\n" +
                               "Key : " + dataReader[6].ToString() + "\r\n" +
                               "Time Stamp : " + dataReader[5].ToString() + "\r\n" + "---------------------------------------------------------" + "\r\n");
                  }
              connection.Close();
              return dataItems;
           }
           catch (Exception msg)
           {
              MessageBox.Show(msg.ToString());
              throw;
           }
        }
        */

    }
}
