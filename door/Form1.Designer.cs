﻿namespace door
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUid = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtStdID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDisplayStdID = new System.Windows.Forms.TextBox();
            this.txtDisplayFirstName = new System.Windows.Forms.TextBox();
            this.txtDisplaySurname = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDisplayDate = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.tbDataItems = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUid
            // 
            this.txtUid.BackColor = System.Drawing.SystemColors.Window;
            this.txtUid.Cursor = System.Windows.Forms.Cursors.No;
            this.txtUid.Location = new System.Drawing.Point(323, 27);
            this.txtUid.Margin = new System.Windows.Forms.Padding(4);
            this.txtUid.Name = "txtUid";
            this.txtUid.ReadOnly = true;
            this.txtUid.Size = new System.Drawing.Size(132, 22);
            this.txtUid.TabIndex = 0;
            this.txtUid.TextChanged += new System.EventHandler(this.txtUid_TextChanged);
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Cursor = System.Windows.Forms.Cursors.No;
            this.txtFirstName.Location = new System.Drawing.Point(93, 66);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(132, 22);
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TextChanged += new System.EventHandler(this.txtFirstName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "First Name";
            // 
            // txtSurname
            // 
            this.txtSurname.BackColor = System.Drawing.SystemColors.Window;
            this.txtSurname.Cursor = System.Windows.Forms.Cursors.No;
            this.txtSurname.Location = new System.Drawing.Point(323, 66);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(4);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.ReadOnly = true;
            this.txtSurname.Size = new System.Drawing.Size(132, 22);
            this.txtSurname.TabIndex = 3;
            this.txtSurname.TextChanged += new System.EventHandler(this.txtSurename_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Surname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(241, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "UID";
            // 
            // txtStdID
            // 
            this.txtStdID.BackColor = System.Drawing.SystemColors.Window;
            this.txtStdID.Cursor = System.Windows.Forms.Cursors.No;
            this.txtStdID.Location = new System.Drawing.Point(93, 27);
            this.txtStdID.Margin = new System.Windows.Forms.Padding(4);
            this.txtStdID.Name = "txtStdID";
            this.txtStdID.ReadOnly = true;
            this.txtStdID.Size = new System.Drawing.Size(132, 22);
            this.txtStdID.TabIndex = 6;
            this.txtStdID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Student ID";
            // 
            // txtDisplayStdID
            // 
            this.txtDisplayStdID.BackColor = System.Drawing.SystemColors.Window;
            this.txtDisplayStdID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDisplayStdID.Cursor = System.Windows.Forms.Cursors.No;
            this.txtDisplayStdID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDisplayStdID.Location = new System.Drawing.Point(7, 145);
            this.txtDisplayStdID.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplayStdID.Multiline = true;
            this.txtDisplayStdID.Name = "txtDisplayStdID";
            this.txtDisplayStdID.ReadOnly = true;
            this.txtDisplayStdID.Size = new System.Drawing.Size(439, 34);
            this.txtDisplayStdID.TabIndex = 8;
            this.txtDisplayStdID.TextChanged += new System.EventHandler(this.txtBoxAll1_TextChanged);
            // 
            // txtDisplayFirstName
            // 
            this.txtDisplayFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDisplayFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDisplayFirstName.Cursor = System.Windows.Forms.Cursors.No;
            this.txtDisplayFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDisplayFirstName.Location = new System.Drawing.Point(7, 175);
            this.txtDisplayFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplayFirstName.Multiline = true;
            this.txtDisplayFirstName.Name = "txtDisplayFirstName";
            this.txtDisplayFirstName.ReadOnly = true;
            this.txtDisplayFirstName.Size = new System.Drawing.Size(439, 34);
            this.txtDisplayFirstName.TabIndex = 9;
            this.txtDisplayFirstName.TextChanged += new System.EventHandler(this.txtBoxAll2_TextChanged);
            // 
            // txtDisplaySurname
            // 
            this.txtDisplaySurname.BackColor = System.Drawing.SystemColors.Window;
            this.txtDisplaySurname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDisplaySurname.Cursor = System.Windows.Forms.Cursors.No;
            this.txtDisplaySurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDisplaySurname.Location = new System.Drawing.Point(7, 207);
            this.txtDisplaySurname.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplaySurname.Multiline = true;
            this.txtDisplaySurname.Name = "txtDisplaySurname";
            this.txtDisplaySurname.ReadOnly = true;
            this.txtDisplaySurname.Size = new System.Drawing.Size(439, 38);
            this.txtDisplaySurname.TabIndex = 10;
            this.txtDisplaySurname.TextChanged += new System.EventHandler(this.txtBoxAll3_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.No;
            this.pictureBox1.Location = new System.Drawing.Point(964, 32);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 135);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.No;
            this.pictureBox2.Location = new System.Drawing.Point(15, 345);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(560, 315);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.No;
            this.pictureBox3.Location = new System.Drawing.Point(644, 345);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(560, 315);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1053, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Real time";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 324);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Picture Form Capture";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(641, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Picture From Data";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtDisplayDate
            // 
            this.txtDisplayDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDisplayDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDisplayDate.Cursor = System.Windows.Forms.Cursors.No;
            this.txtDisplayDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDisplayDate.Location = new System.Drawing.Point(7, 242);
            this.txtDisplayDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplayDate.Multiline = true;
            this.txtDisplayDate.Name = "txtDisplayDate";
            this.txtDisplayDate.ReadOnly = true;
            this.txtDisplayDate.Size = new System.Drawing.Size(439, 38);
            this.txtDisplayDate.TabIndex = 19;
            this.txtDisplayDate.TextChanged += new System.EventHandler(this.txtDisplayDate_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2.Cursor = System.Windows.Forms.Cursors.No;
            this.textBox2.Location = new System.Drawing.Point(287, 106);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(160, 22);
            this.textBox2.TabIndex = 13;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(241, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Time";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 106);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "Expire Date";
            // 
            // txtDate
            // 
            this.txtDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDate.Cursor = System.Windows.Forms.Cursors.No;
            this.txtDate.Location = new System.Drawing.Point(93, 103);
            this.txtDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(132, 22);
            this.txtDate.TabIndex = 21;
            this.txtDate.TextChanged += new System.EventHandler(this.txtDate_TextChanged);
            // 
            // tbDataItems
            // 
            this.tbDataItems.BackColor = System.Drawing.SystemColors.Window;
            this.tbDataItems.Cursor = System.Windows.Forms.Cursors.No;
            this.tbDataItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDataItems.Location = new System.Drawing.Point(474, 22);
            this.tbDataItems.Margin = new System.Windows.Forms.Padding(4);
            this.tbDataItems.Multiline = true;
            this.tbDataItems.Name = "tbDataItems";
            this.tbDataItems.ReadOnly = true;
            this.tbDataItems.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbDataItems.Size = new System.Drawing.Size(464, 289);
            this.tbDataItems.TabIndex = 22;
            this.tbDataItems.TextChanged += new System.EventHandler(this.tbDataItems_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 673);
            this.Controls.Add(this.tbDataItems);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtDisplayDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtDisplaySurname);
            this.Controls.Add(this.txtDisplayFirstName);
            this.Controls.Add(this.txtDisplayStdID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtStdID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSurname);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.txtUid);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUid;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtStdID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDisplayStdID;
        private System.Windows.Forms.TextBox txtDisplayFirstName;
        private System.Windows.Forms.TextBox txtDisplaySurname;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDisplayDate;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox tbDataItems;
    }
}

